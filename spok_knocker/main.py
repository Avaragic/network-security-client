#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import argparse
import keytool
import knock_config
from exception import *
from logging.config import dictConfig
import os
import json

__author__ = 'Kien Truong'


def gen_key(args):
    try:
        key = keytool.gen_key(args.key_size)
        print key
    except InvalidKeyException, e:
        print e.message


def knock(args):
    try:
        knock_configs = knock_config.read_knock_configs(args.conf_file)
        config_name = args.config.lower()
        if config_name in knock_configs:
            knock_configs[config_name].execute(args.destination, args.interval)
        else:
            available = "|".join(knock_configs.keys())
            print "Invalid knock configuration"
            print "Available configuration are: {configs}"\
                .format(configs=available)
    except InvalidConfigurationException, e:
        print "Your configuration file is invalid"
        print e.message
    except InvalidKeyException , e:
        print e.message


def setup_logging(path='log_config.json'):
    if os.path.exists(path):
        with open(path, 'rb') as logfile:
            logconfig = json.load(logfile)
            dictConfig(logconfig)


def main():
    # Do argument parsing
    parser = argparse.ArgumentParser()
    sub_parsers = parser.add_subparsers(
        title='Available sub-commands',
        description='The sub-commands of the client',
        help='You can use one of these to select the sub-commands'
    )

    # Argument for keygen function
    keygen_parser = sub_parsers.add_parser('keygen')
    keygen_parser.add_argument(
        '-s', '--size',
        dest='key_size',
        help='The size of the key to generated in bytes. Default: 20 bytes',
        default=20,
        type=int
    )
    keygen_parser.set_defaults(func=gen_key)

    # Argument for knock function
    knock_parser = sub_parsers.add_parser('knock')
    knock_parser.add_argument(
        '-c', '--conf',
        dest='conf_file',
        help='The path to the knock configuration file',
        required=True
    )
    knock_parser.add_argument(
        '-p', '--profile',
        dest='config',
        help='The knock configuration to used',
        required=True
    )
    knock_parser.add_argument(
        '-d', '--destination',
        dest='destination',
        help='The address of the target machine',
        required=True
    )
    knock_parser.add_argument(
        '-i', '--interval',
        dest='interval',
        default=1,
        help='The interval between knock in second. Default: 1s'
    )
    knock_parser.set_defaults(func=knock)

    # Setup logging
    setup_logging()

    # Parse the argument and run the corresponding function
    parse_result = parser.parse_args()
    parse_result.func(parse_result)

